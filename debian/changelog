tepl (5.0.0-1) UNRELEASED; urgency=medium

  * New upstream release
  * debian/control{,.in}:
    - Bump libglib2.0-dev build dependency requirement to 2.64
    - Remove the following build dependencies: libuchardet-dev and libxml2-dev
    - Add the following build dependencies: libicu-dev, meson, pkg-config
    - Rename binary packages to account for new major version and soname bump
    - Add the following runtime dependencies to the libtepl-5-dev package, to
      reflect the contents of the pc file and the public headers:
      libamtk-5-dev, libglib2.0-dev, libgtk-3-dev, libgtksourceview-4-dev
  * debian/copyright: update the upstream license to LPGL 3 or later
  * debian/libtepl-5-dev.doc-base: Change the doc-base document ID to tepl5
    to avoid conflicts
  * debian/libtepl-5-0.symbols: Update symbols

 -- Olivier Tilloy <olivier.tilloy@canonical.com>  Tue, 29 Sep 2020 18:07:52 +0200

tepl (4.4.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.5.0 (no further changes)
  * Bump debhelper compatibility to 12
  * debian/libtepl-4-dev.doc-base: Use the paths from /usr/share/doc, to
    please lintian
  * debian/control.in: Add several -doc packages to the build-dependencies so
    the links between the documentations are resolved properly

 -- Laurent Bigonville <bigon@debian.org>  Sun, 08 Mar 2020 20:37:45 +0100

tepl (4.3.1-1) unstable; urgency=medium

  * New upstream release
  * Update package descriptions to match the upstream documentation update
    from GTK+ to GTK:
    - debian/control{,.in}
  * Add new public functions to the symbol file:
    - debian/libtepl-4-0.symbols

 -- Olivier Tilloy <olivier.tilloy@canonical.com>  Mon, 17 Feb 2020 15:30:06 +0000

tepl (4.2.0-2) unstable; urgency=medium

  * Set HOME to fix dh_auto_test (Closes: #916363)
  * Add -Wl,-O1 to our LDFLAGS

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 13 Dec 2018 20:16:44 -0500

tepl (4.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #895688)

 -- Tanguy Ortolo <tanguy+debian@ortolo.eu>  Wed, 10 Oct 2018 17:04:39 +0200
