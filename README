Tepl - Text editor product line
===============================

This is version 5.0.0 of Tepl.

Tepl is a library that eases the development of GtkSourceView-based text
editors and IDEs.

Tepl was previously named Gtef (GTK+ text editor framework). The project has
been renamed in June 2017 to have a more beautiful name. The end of Tepl is
pronounced like in “apple”.

The Tepl library is free software and is released under the terms of the GNU
Lesser General Public License, see the 'LICENSES/' directory for more
information.

The Tepl web page:

    https://wiki.gnome.org/Projects/Tepl

Dependencies
------------

- GLib >= 2.64
- GTK >= 3.22
- GtkSourceView >= 4.0
- Amtk >= 5.0 - https://wiki.gnome.org/Projects/Amtk
- ICU - http://site.icu-project.org/

Installation
------------

Simple install procedure from a tarball:

  $ ./configure
  $ make
  [ Become root if necessary ]
  $ make install

See the file 'INSTALL' for more detailed information.

From the Git repository, the 'configure' script and the 'INSTALL' file are not
yet generated, so you need to run 'autogen.sh' instead, which takes the same
arguments as 'configure'.

To build the latest version of Tepl plus its dependencies from Git, Jhbuild is
recommended:

    https://wiki.gnome.org/Projects/Jhbuild

How to contribute
-----------------

See the file 'HACKING'.
